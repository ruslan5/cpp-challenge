#include <iostream>
#include <windows.h>

using namespace std; 

const unsigned short int BOARD_WIDTH = 1;
const unsigned short int WIDTH = 40;
const unsigned short int HEIGHT = 20;
const unsigned short int GROUND_LEVEL = HEIGHT - BOARD_WIDTH - 1;
unsigned short int playerX = BOARD_WIDTH;
unsigned short int playerY = GROUND_LEVEL;
const char PLAYER_SYMBOL = 'A';
const float FPS = 60;
bool backgroundRendered = false;
HANDLE stdOutputHandle;
const int JUMP_HEIGHT = 10;
unsigned long long incrementPleyerYAt = 0;
unsigned long long sideButtonPressedLastCheckedAt = 0;
unsigned long long sideButtonPressedAt = 0;
unsigned long long sideButtonPressedDuration = 0;
bool sideButtonPressed = false;
bool playerUp = false;
enum SIDE_MOVING {NONE = 1, LEFT, RIGHT};
unsigned long long moveAt = 0;
unsigned short int moveToX = 0;


bool isBoard(int x, int y) {

	if (BOARD_WIDTH) {

		if (x < BOARD_WIDTH || x >= WIDTH - BOARD_WIDTH) {

			return true;
		} else if (y < BOARD_WIDTH || y >= HEIGHT - BOARD_WIDTH) {

			return true;
		}
	}

	return false;
}

bool isPlayerPosition(int x, int y) {
	return y == playerY && x == playerX;
}

void renderBackground() {
	const char BOARD_SYMBOL = '#';

	for (int i = 0; i < HEIGHT; i++) {

		for (int j = 0; j < WIDTH; j++) {

			if (isBoard(j, i)) {
				cout << BOARD_SYMBOL;
			} else {
				cout << ' ';
			}
		}

		cout << endl;
	}

	backgroundRendered = true;
}

HANDLE getStdOutputHandle() {
	if (!stdOutputHandle) {
		stdOutputHandle = GetStdHandle(STD_OUTPUT_HANDLE);
		CONSOLE_CURSOR_INFO structCursorInfo;

		GetConsoleCursorInfo(stdOutputHandle, &structCursorInfo);

		structCursorInfo.bVisible = FALSE;

		SetConsoleCursorInfo(stdOutputHandle, &structCursorInfo);
	}

	return stdOutputHandle;
}

void renderPlayer() {
	HANDLE stdOutputHandle = getStdOutputHandle();

	for (unsigned short int i = playerY - 1; i < playerY + 2; i++) {

		for (unsigned short int j = playerX - 1; j < playerX + 2; j++) {
			
			if (i >= 0 && i < HEIGHT && j >= 0 && j < WIDTH) {
				
				if (!isBoard(j, i)) {

					SetConsoleCursorPosition(stdOutputHandle, { (short)j, (short)i });

					if (i == playerY && j == playerX) {
						cout << PLAYER_SYMBOL;
					} else {
						cout << ' ';
					}
				}
			}
		}
	}
}

void render() {
	if (!backgroundRendered) {
		renderBackground();
	}

	renderPlayer();
}

int getJumpStepDelay() {
	int minMilisecods = 25;
	int steps = HEIGHT - BOARD_WIDTH - playerY;

	return minMilisecods + 7 * steps;
}

void handleJump() {
	unsigned long long now = GetTickCount64();

	if (incrementPleyerYAt && now >= incrementPleyerYAt) {
		incrementPleyerYAt = now + getJumpStepDelay();

		if (playerUp) {
			playerY--;
		} else {
			playerY++;
		}

		if (playerY == GROUND_LEVEL) {
			incrementPleyerYAt = 0;
			playerUp = false;
		} else if (playerY <= GROUND_LEVEL - JUMP_HEIGHT) {
			playerY = GROUND_LEVEL - JUMP_HEIGHT;
			playerUp = false;
		}
	}
}

void handleSideMoving(SIDE_MOVING way) {
	unsigned long long now = GetTickCount64();
	unsigned long long difference = now - sideButtonPressedLastCheckedAt;

	if (!sideButtonPressed) {
		sideButtonPressedAt = 0;
	}

	if (difference > 250) {
		sideButtonPressed = false;
	} else {
		sideButtonPressed = true;
	}

	if (!sideButtonPressedAt && sideButtonPressed) {
		sideButtonPressedAt = sideButtonPressedLastCheckedAt;
	}

	if (sideButtonPressed) {
		int delay = 400;
		sideButtonPressedDuration += now - sideButtonPressedAt;

		if (sideButtonPressedDuration >= 500 && sideButtonPressedDuration <= 4000) {
			delay = 20 * (10000 / sideButtonPressedDuration);
		} else if (sideButtonPressedDuration > 4000) {
			delay = 50;
		}
		
		if (way != NONE && !moveAt) {

			if (way == RIGHT && !isBoard(playerX + 1, playerY)) {
				moveAt = now + delay;
				moveToX = playerX + 1;
			}

			if (way == LEFT && !isBoard(playerX - 1, playerY)) {
				moveAt = now + delay;
				moveToX = playerX - 1;
			}
		}

	} else {
		sideButtonPressedDuration = 0;
	}

	if (moveAt && now >= moveAt) {
		moveAt = 0;
		playerX = moveToX;
	}
}

void input() {
	unsigned long long now = GetTickCount64();
	SIDE_MOVING way = NONE;

	if (GetAsyncKeyState(VK_UP)) {

		if (playerY == GROUND_LEVEL && !incrementPleyerYAt) {
			incrementPleyerYAt = now + getJumpStepDelay();
			playerUp = true;
		}
	}

	if (GetAsyncKeyState(VK_LEFT)) {
		sideButtonPressedLastCheckedAt = now;

		way = LEFT;
	}

	if (GetAsyncKeyState(VK_RIGHT)) {
		sideButtonPressedLastCheckedAt = now;

		way = RIGHT;
	}

	handleJump();
	handleSideMoving(way);
}

int main() {
	while (true) {
		render();
		input();
	}

	return 0;
}